<?php

class Category extends Eloquent {

  protected $table = 'categories';
  public $timestamps = false;
  
  public function shirts()
  {
    return $this->hasMany('Shirt');
  }
}
