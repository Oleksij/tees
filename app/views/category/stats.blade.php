@extends('layouts.default')
@section('content')

<h1>Stats</h1>

<div class="col-md-9">
	<table class="table table-striped js-shirts-table">
		<thead>
			<tr>
				<th>Image</th>
				<th>Clicks</th>
				<th>Sales</th>
				<th>Percents</th>
				<th>Link</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($shirts as $shirt)
				<tr>
					<td>{{ HTML::image($shirt->img, "", array('class' => "stats-image")) }}</td>
					<td>{{ $shirt->clicks }}</td>
					<td>{{ $shirt->sales }}</td>
					<td>
						<?php 
							echo $shirt->clicks > 0 ? round($shirt->sales / $shirt->clicks) : 0;	
						?>%
					</td>
					<td>
						{{ HTML::link('/shirt/' . $shirt->url, null, array("target" => "_blank"), false) }}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="col-md-3">
</div>

@stop