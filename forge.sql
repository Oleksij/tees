/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50525
Source Host           : localhost:3306
Source Database       : forge

Target Server Type    : MYSQL
Target Server Version : 50525
File Encoding         : 65001

Date: 2014-06-08 13:51:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slogan` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `visits` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('10', 'test', 'test', 'test', '419f22', '4', '2014-06-08 10:26:37');
INSERT INTO `categories` VALUES ('12', 'test1', 'test1', 'test1', '32f002', '2', '2014-06-08 10:29:55');

-- ----------------------------
-- Table structure for `shirts`
-- ----------------------------
DROP TABLE IF EXISTS `shirts`;
CREATE TABLE `shirts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `origin_url` varchar(255) NOT NULL,
  `clicks` int(11) NOT NULL,
  `sales` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shirts
-- ----------------------------
INSERT INTO `shirts` VALUES ('15', '10', 'Furious Pete \'Stay Hungry\' Tee', '           The Furious Apparel \"<i>STAY HUNGRY\"</i> shirt has been in the works for awhile and will be exclusively available here for a limited time! <br><br>Stay Hungry has been a saying that Furious Pete has used for a long time and it not only refers to your appetite at the dinner table but also with life itself! Stay hungry with all your goals and dreams! <br><br><b>Stay Hungry Team Furious!</b><br><br>Check out the Toddler \'I\'m Hungry\' Tee as well! <br><a href=\"http://teespring.com/furioushungry\">http://teespring.com/furioushungry</a><br><br><br>         ', 'http://images.teespring.com/shirt_pic/931352/183658/front.jpg?v=2014-06-03-00-47', 'f349ef', 'http://teespring.com/stayhungry', '0', '350', '0000-00-00 00:00:00');
INSERT INTO `shirts` VALUES ('16', '10', 'LIMITED EDITION - Mac\'s Brown Bottle Ale', '           <b>Best served warm.<br></b>Fine print on label reads \"Brewed on neutral ground in Chicago, IL\"<br><br>Check out the re-launched <b>original</b> KCF tee, back by popular demand!<br><a href=\"http://teespring.com/fuego\">Keep Calm and FUEGO!</a><br><br>Order for family &amp; friends and save on shipping costs.  For larger sizes up to 5XL, select the Fruit of the Loom option below.<br><i><br></i><i>Silkscreen printed for quality and longevity. 100% customer satisfaction </i><a href=\"http://www.teespring.com/about/terms\"><i>GUARANTEED</i></a><i>.  This shirt was created by fans, for fans. ©Proud Pixel Productions, LLC.</i><b><br></b><b><br></b>         ', 'http://images.teespring.com/shirt_pic/923750/173424/front.jpg?v=2014-05-31-17-21', '49f4ce', 'http://teespring.com/macsbrownbottle', '0', '503', '0000-00-00 00:00:00');
INSERT INTO `shirts` VALUES ('18', '10', 'Furious Pete \'Stay Hungry\' Tee', '           The Furious Apparel \"<i>STAY HUNGRY\"</i> shirt has been in the works for awhile and will be exclusively available here for a limited time! <br><br>Stay Hungry has been a saying that Furious Pete has used for a long time and it not only refers to your appetite at the dinner table but also with life itself! Stay hungry with all your goals and dreams! <br><br><b>Stay Hungry Team Furious!</b><br><br>Check out the Toddler \'I\'m Hungry\' Tee as well! <br><a href=\"http://teespring.com/furioushungry\">http://teespring.com/furioushungry</a><br><br><br>         ', 'http://images.teespring.com/shirt_pic/931352/183658/front.jpg?v=2014-06-03-00-47', '78690f', 'http://teespring.com/stayhungry', '0', '350', '0000-00-00 00:00:00');
INSERT INTO `shirts` VALUES ('19', '10', 'Furious Pete \'Stay Hungry\' Tee', '           The Furious Apparel \"<i>STAY HUNGRY\"</i> shirt has been in the works for awhile and will be exclusively available here for a limited time! <br><br>Stay Hungry has been a saying that Furious Pete has used for a long time and it not only refers to your appetite at the dinner table but also with life itself! Stay hungry with all your goals and dreams! <br><br><b>Stay Hungry Team Furious!</b><br><br>Check out the Toddler \'I\'m Hungry\' Tee as well! <br><a href=\"http://teespring.com/furioushungry\">http://teespring.com/furioushungry</a><br><br><br>         ', 'http://images.teespring.com/shirt_pic/931352/183658/front.jpg?v=2014-06-03-00-47', 'bea415', 'http://teespring.com/stayhungry', '0', '350', '0000-00-00 00:00:00');
INSERT INTO `shirts` VALUES ('24', '12', 'Only Elephants Need Ivory', '           <b>AVAILABLE THROUGH TUESDAY, JUNE 10 ONLY!! </b><b>  </b>Grab your limited edition, screen printed tee or tank. Sales will fund the startup <a href=\"http://www.golfingforelephants.com\">Golfing for Elephants L3C</a>, a  responsible golf tourism company that will donate 50% of its profits to  anti-poaching efforts in Kenya. Golfing for Elephants is proud to be a partner with <a href=\"http://www.96elephants.org/\">96 Elephants</a>, a campaign of <a href=\"http://www.wcs.org/\">Wildlife Conservation Society</a>. When you buy this shirt, your name will be listed on our  upcoming ivory-free pledge page at <a href=\"http://www.goivoryfree.com\">goivoryfree.com</a>. Available in Elephant Grey and a variety of colors and styles. Select Style Drop-down below to view all styles of shirts available. Each shirt is printed on high quality material and we always offer a FULL money back guarantee! Order 2 or more and save on shipping costs. Please note the Hanes Tagless Tee \"chest width\" measures armhole to armhole across the shirt, not the person. See  <a href=\"http://www.customink.com/items/sizing/116200_lineup/standard.htm\">http://www.customink.com/items/sizing/116200_lineup/standard.htm</a>         ', 'http://images.teespring.com/shirt_pic/838723/107767/front.jpg?v=2014-05-16-17-02', 'fb6efa', 'http://teespring.com/goivoryfree', '0', '64', '0000-00-00 00:00:00');
INSERT INTO `shirts` VALUES ('25', '12', 'Only Elephants Need Ivory', '           <b>AVAILABLE THROUGH TUESDAY, JUNE 10 ONLY!! </b><b>  </b>Grab your limited edition, screen printed tee or tank. Sales will fund the startup <a href=\"http://www.golfingforelephants.com\">Golfing for Elephants L3C</a>, a  responsible golf tourism company that will donate 50% of its profits to  anti-poaching efforts in Kenya. Golfing for Elephants is proud to be a partner with <a href=\"http://www.96elephants.org/\">96 Elephants</a>, a campaign of <a href=\"http://www.wcs.org/\">Wildlife Conservation Society</a>. When you buy this shirt, your name will be listed on our  upcoming ivory-free pledge page at <a href=\"http://www.goivoryfree.com\">goivoryfree.com</a>. Available in Elephant Grey and a variety of colors and styles. Select Style Drop-down below to view all styles of shirts available. Each shirt is printed on high quality material and we always offer a FULL money back guarantee! Order 2 or more and save on shipping costs. Please note the Hanes Tagless Tee \"chest width\" measures armhole to armhole across the shirt, not the person. See  <a href=\"http://www.customink.com/items/sizing/116200_lineup/standard.htm\">http://www.customink.com/items/sizing/116200_lineup/standard.htm</a>         ', 'http://images.teespring.com/shirt_pic/838723/107767/front.jpg?v=2014-05-16-17-02', '941a3b', 'http://teespring.com/goivoryfree', '2', '64', '0000-00-00 00:00:00');
INSERT INTO `shirts` VALUES ('26', '12', 'Only Elephants Need Ivory', '           <b>AVAILABLE THROUGH TUESDAY, JUNE 10 ONLY!! </b><b>  </b>Grab your limited edition, screen printed tee or tank. Sales will fund the startup <a href=\"http://www.golfingforelephants.com\">Golfing for Elephants L3C</a>, a  responsible golf tourism company that will donate 50% of its profits to  anti-poaching efforts in Kenya. Golfing for Elephants is proud to be a partner with <a href=\"http://www.96elephants.org/\">96 Elephants</a>, a campaign of <a href=\"http://www.wcs.org/\">Wildlife Conservation Society</a>. When you buy this shirt, your name will be listed on our  upcoming ivory-free pledge page at <a href=\"http://www.goivoryfree.com\">goivoryfree.com</a>. Available in Elephant Grey and a variety of colors and styles. Select Style Drop-down below to view all styles of shirts available. Each shirt is printed on high quality material and we always offer a FULL money back guarantee! Order 2 or more and save on shipping costs. Please note the Hanes Tagless Tee \"chest width\" measures armhole to armhole across the shirt, not the person. See  <a href=\"http://www.customink.com/items/sizing/116200_lineup/standard.htm\">http://www.customink.com/items/sizing/116200_lineup/standard.htm</a>         ', 'http://images.teespring.com/shirt_pic/838723/107767/front.jpg?v=2014-05-16-17-02', '6de329', 'http://teespring.com/goivoryfree', '0', '64', '0000-00-00 00:00:00');
INSERT INTO `shirts` VALUES ('27', '12', 'Only Elephants Need Ivory', '           <b>AVAILABLE THROUGH TUESDAY, JUNE 10 ONLY!! </b><b>  </b>Grab your limited edition, screen printed tee or tank. Sales will fund the startup <a href=\"http://www.golfingforelephants.com\">Golfing for Elephants L3C</a>, a  responsible golf tourism company that will donate 50% of its profits to  anti-poaching efforts in Kenya. Golfing for Elephants is proud to be a partner with <a href=\"http://www.96elephants.org/\">96 Elephants</a>, a campaign of <a href=\"http://www.wcs.org/\">Wildlife Conservation Society</a>. When you buy this shirt, your name will be listed on our  upcoming ivory-free pledge page at <a href=\"http://www.goivoryfree.com\">goivoryfree.com</a>. Available in Elephant Grey and a variety of colors and styles. Select Style Drop-down below to view all styles of shirts available. Each shirt is printed on high quality material and we always offer a FULL money back guarantee! Order 2 or more and save on shipping costs. Please note the Hanes Tagless Tee \"chest width\" measures armhole to armhole across the shirt, not the person. See  <a href=\"http://www.customink.com/items/sizing/116200_lineup/standard.htm\">http://www.customink.com/items/sizing/116200_lineup/standard.htm</a>         ', 'http://images.teespring.com/shirt_pic/838723/107767/front.jpg?v=2014-05-16-17-02', 'a19a12', 'http://teespring.com/goivoryfree', '0', '64', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'Oleks@mail.ru', '$2y$10$SlS7wlCCixggRIAC0v8g.e/dqGNNl.xclyS7szJ4Wc45EMTauvEvG', 'bQdft5HSBHIyAibPSjuNLYWwXPMb2nDo6sVJwzDdHln8DCBMv6LYznd1EncQ');
