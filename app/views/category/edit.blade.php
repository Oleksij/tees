@extends('layouts.default')
@section('content')
	
	<h1>Edit category</h1>
	{{Form::model($category, ['url' => array('/admin/category/edit', $category->id) ])}}
	<div class="form-group">
		{{ Form::label('name', 'Campaign name') }}
		{{ Form::text('name',null, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('slogan', 'Headline') }}
		{{ Form::text('slogan',null, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('description', 'Description') }}
		{{ Form::text('description',null, array('class' => 'form-control')) }}
	</div>
	<div class="form-actions">
		{{ Form::submit('Edit campaign', array('class' => 'btn btn-primary js-add-campaign', 'name' => 'edit')) }}
	</div>
	{{ Form::close() }}
@stop
