@extends('layouts.default')
@section('content')
	<h1>Shirts site</h1>

	<div class="col-md-9">
		<div class="row">
			<?php $i = 0; ?>
			@foreach( $shirts as $shirt )
				<?php $i++; ?>
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="well text-center">
						<a href="{{ URL::to('/shirt/' . $shirt->url) }}">{{ HTML::image($shirt->img, "", array('class' => "img-responsive")) }}</a>
						<h4>{{ $shirt->name }}</h4>
						{{ HTML::link( '/shirt/' . $shirt->url, 'More info', array( "class" => "btn btn-primary btn-lg btn-block"), false ) }}
					</div>
				</div>
				<?php if($i % 4 == 0)  { ?>
					</div><div class="row">
				<?php } ?>
			@endforeach
		</div>
	</div>
	<div class="col-md-3">
		<ul class="nav nav-pills nav-stacked">
		  @foreach($categories as $category)
			<li>{{ HTML::link( '/' . $category->url, $category->name, false ) }}</li>
		  @endforeach
		</ul>
	</div>
	
@stop