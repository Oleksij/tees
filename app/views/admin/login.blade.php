@extends('layouts.default')
@section('content')
  <h1>Please Log in</h1>
  {{ Form::open(array('url' => 'admin')) }}
  <div class="col-md-5">
    <div class="form-group">
      {{ Form::label('username', 'Username') }}
      {{ Form::text('username', '', array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
      {{ Form::label('password', 'Password') }}
      {{ Form::password('password', array('class' => 'form-control')) }}
    </div>
    <div class="form-actions form-group">
      {{ Form::submit('Login', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
  {{ Form::close() }}
@stop