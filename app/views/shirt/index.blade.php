@extends('layouts.default')
@section('content')

	<h1>
		<a href="#" class='btn btn-success js-add-shirts'>Add shirts &#8595;</a>
	</h1>
	
	<div class='js-add-shirts-container' style="display: none" data-category-id="{{ $category }}">
			<div class="form-group js-shirts-inputs">
				<input type="text" class="js-shirt-origin-url" class="form-control">
				<button class="btn btn-success js-plus-shirt">+</button>
			</div>

			<div class="form-group">
				<button class="btn btn-primary js-add-shirt-go">Add shirts</button>
			</div>
	</div>

	<table class="table table-striped js-shirts-table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>URL</th>
				<th>Origin URL</th>
				<th>Actions</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($shirts as $shirt)
				<tr class="js-shirts-row">
					<td>{{ $shirt->name }}</td>
					<td>{{ $shirt->description }}</td>
					<td>{{ HTML::link('/shirt/' . $shirt->url, null, array("target" => "_blank"), false) }}</td>
					<td class="js-origin-url" data-id="{{ $shirt->id }}">
						<span class='js-origin-current'>{{ HTML::link($shirt->origin_url, null, array("target" => "_blank"), false) }}</span>
					</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Actions <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>{{ HTML::link('/shirt/' . $shirt->url, 'View', array("target" => "_blank"), false) }}</li>
								<li><a class='js-edit-shirt'>Edit</a></li>
								<li><a class='js-delete-shirt'>Delete</a></li>
							</ul>
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop