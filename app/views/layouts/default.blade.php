<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Shirts</title>

	{{ HTML::script('js/jquery.js'); }}
	{{ HTML::script('js/bootstrap.js'); }}
	{{ HTML::script('js/main.js'); }}
	{{ HTML::style('css/bootstrap.css'); }}
	{{ HTML::style('css/style.css'); }}

</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Shirts</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li>{{ HTML::link( '/', 'Home', true ) }}</li>
            @if(Auth::check())
            <li>{{ HTML::link( '/admin/home', 'Control Panel', true ) }}</li>
            <li>{{ HTML::link( '/admin/logout', 'Logout ('.Auth::user()->username.')', true ) }}</li>
            @else
            <li>{{ HTML::link( '/admin', 'Login', true ) }}</li>
            @endif
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container js-main" data-domain="{{ URL::to('/') }}">
      
      <div class="main-content">
      		@if(Session::has('success'))
	      	    <div class="alert alert-success">
	      	      <button type="button" class="close" data-dismiss="alert">×</button>
	      	      {{ Session::get('success') }}
	      	    </div>    
	      	    @endif
	      	 
	      	    @if(Session::has('error'))
	      	    <div class="alert alert-danger">
	      	      <button type="button" class="close" data-dismiss="alert">×</button>
	      	      {{ Session::get('error') }}
	      	    </div>    
      	    @endif
      	@yield('content')
      </div>

	  <div class="clearfix"></div>

      <hr>

      <footer>
        <p>© A La Carte 2014</p>
      </footer>
    </div>

</body>
</html>