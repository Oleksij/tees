@extends('layouts.default')
@section('content')
	
	<h1>
		<a href="#" class='btn btn-success js-add-campaign'>Add campaign &#8595;</a>
	</h1>
	
	<div class='js-add-campaign-container' style="display: none">
		{{ Form::open(array('url' => 'admin/category/add', "class" => "form-inline")) }}
			<div class="form-group">
				{{ Form::text('name', '', array('class' => 'form-control' , 'placeholder' => 'Name')) }}
			</div>
			<div class="form-group">
				{{ Form::text('slogan', '', array('class' => 'form-control', 'placeholder' => 'Slogan' )) }}
			</div>
			<div class="form-group">
				{{ Form::text('description', '', array('class' => 'form-control', 'placeholder' => 'Description')) }}
			</div>
			<div class="form-group">
				{{ Form::submit('Add campaign', array('class' => 'btn btn-primary')) }}
			</div>
		{{ Form::close() }}
	</div>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Slogan</th>
				<th>Decription</th>
				<th>Edit</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($categories as $category)
				<tr>
					<td>{{ $category->name }}</td>
					<td>{{ $category->slogan }}</td>
					<td>{{ $category->description }}</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Actions <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>{{ HTML::link( '/admin/category/stats/' . $category->id, 'Stats', true ) }}</li>
								<li>{{ HTML::link( '/admin/shirt/' . $category->id, 'Edit Shirt', true ) }}</li>
								<li>{{ HTML::link( '/admin/category/edit/' . $category->id, 'Edit Campaign', true ) }}</li>
								<li>{{ HTML::link( '/admin/category/delete/' . $category->id, 'Delete Campaign', true ) }}</li>
							</ul>
						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop