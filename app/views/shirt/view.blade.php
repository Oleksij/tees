@extends('layouts.default')
@section('content')


	<div class="col-md-4">
		<div class="row">
			<h1 class="product-name">{{ $shirt[0]->name }}</h1>
			<img src="{{$shirt[0]->img}}" alt="" class="img-responsive">
		</div>
	</div>
	<div class="col-md-8">
		{{ $shirt[0]->description }}
	</div>
		
@stop