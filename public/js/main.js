$(document).ready(function() {
	var originalAddShirtContainer = '<div class="form-group js-shirts-inputs"><input type="text" class="js-shirt-origin-url" class="form-control"><button class="btn btn-success js-plus-shirt">+</button></div><div class="form-group"><button class="btn btn-primary js-add-shirt-go">Add shirts</button></div>',
		domain = $(".js-main").attr('data-domain');

	$(".js-add-campaign").on('click', function() {
		$(".js-add-campaign-container").toggle();
	});

	$(".js-add-shirts").on('click', function() {
		$(".js-add-shirts-container").toggle();
	});

	$(".js-add-shirts-container").on('click', ".js-plus-shirt", function() {
		$(this).parent().after('<div class="form-group"><input type="text" class="js-shirt-origin-url" class="form-control"><button class="btn btn-success js-plus-shirt">+</button></div>');
		$(this).remove();
	});

	$(".js-add-shirts-container").on('click', ".js-add-shirt-go", function() {
		console.log("go");
		var shirts = [],
		opUrl = domain + '/admin/shirt/add';
		for(var i = 0; i < $(".js-shirt-origin-url").length; i++) {
			if($(".js-shirt-origin-url").val() != '') {
				shirts.push($(".js-shirt-origin-url").val());
			}
		}

		$.ajax({
			type: 'POST',
			url: opUrl,
			data: "shirts=" + JSON.stringify(shirts) + '&id=' + $('.js-add-shirts-container').attr('data-category-id'),
			success: shirtAddedSuccess,
			beforeSend: shirtAddedSuccessBefore,
			error: shirtAddedError
		});

		function shirtAddedSuccess (data) {
			$('.js-add-shirts-container').html(originalAddShirtContainer).append('<div class="alert alert-success">Shirts was successfully added</div>');
			var table = $(".js-shirts-table tbody");
			var newShirts = JSON.parse(data);
			console.log(newShirts);
			for(var i = 0; i < newShirts.length; i++) {
				var row = '<tr class="js-shirts-row"><td>' + newShirts[i].name + '</td><td>' + newShirts[i].description + '</td><td>' + newShirts[i].url + '</td><td class="js-origin-url" data-id="' + newShirts[i].id + '"><span class="js-origin-current">' + newShirts[i].origin_url + '</span></td><td><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu"><li><a class="js-edit-shirt">Edit</a></li><li><a class="js-delete-shirt">Delete</a></li></ul></div></td></tr>';
				table.append(row);
			}
		}
		function shirtAddedError(data) {
			$('.js-add-shirts-container').html(originalAddShirtContainer).append('<div class="alert alert-danger">Oh no! Something went wrong ;(</div>');
			console.log(data);
		}
		function shirtAddedSuccessBefore() {
			$(".js-add-shirt-go").html('<img src="' + domain + '/img/preloader1.gif">');
		}
	});

	$(".js-shirts-table").on('click', ".js-edit-shirt", function() {
		// $(this).parents('tr').find('.js-origin-url').html();
		$(this).parents('tr').find('.js-origin-current').hide();
		$(this).parents('tr').find('.js-origin-url').append('<div class="js-insert-new-origin"><input type="text" class="js-origin-url-new"><button class="js-origin-url-new-go btn btn-success">Edit</button><button class="js-origin-url-cancel btn btn-danger">Cancel</button></div>');
	});

	$(".js-shirts-table").on('click', ".js-origin-url .js-origin-url-cancel", function() {
		$(this).parent().remove();
	}).on('click', ".js-origin-url .js-origin-url-new-go", function() {
		var button = $(this);
		var currentRow = $(this).parents('.js-shirts-row');
		var id = $(this).parents(".js-origin-url").attr('data-id'),
			opUrl = domain + '/admin/shirt/edit';
		$.ajax({
			type: 'POST',
			url: opUrl,
			data: "shirt_id=" + id + "&origin_url=" + $(this).parents(".js-origin-url").find(".js-origin-url-new").val(),
			success: shirtEditedSuccess,
			beforeSend: shirtEditedSuccessBefore,
			error: shirtEditedError
		});

		function shirtEditedSuccess(data) {
			var editedShirt = JSON.parse(data);
			if(editedShirt.status == 'success') {
				console.log("true");
				var tds = currentRow.find('td');
				$(tds[0]).html(editedShirt.name);
				$(tds[1]).html(editedShirt.description);
				$(tds[2]).html(editedShirt.url);
				$(tds[3]).find('.js-origin-current').show().html(editedShirt.origin_url);
				$(tds[3]).find('.js-insert-new-origin').remove();
			}
		}

		function shirtEditedSuccessBefore() {
			button.html("Loading...");
		}

		function shirtEditedError() {
			button.parents('.js-insert-new-origin').remove();
		}
	});

	$(".js-shirts-table").on("click", ".js-delete-shirt", function() {
		var button = $(this);
		var id = $(this).parents('.js-shirts-row').find(".js-origin-url").attr('data-id'),
			opUrl = domain + '/admin/shirt/delete';
		$.ajax({
			type: 'POST',
			url: opUrl,
			data: "shirt_id=" + id,
			success: shirtDeletedSuccess
		});

		function shirtDeletedSuccess(data) {
			var result = JSON.parse(data);
			if(result.status == "success") {
				var row = button.parents(".js-shirts-row");
				row.fadeOut(1000);
				setTimeout(function() {
					row.remove()
				}, 1000);
			}
		}
	});
});