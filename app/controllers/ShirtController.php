<?php
class ShirtController extends BaseController {

	public function Index($id)
	{
		$shirts = Shirt::where('category_id', '=', $id)->get();
		return View::make('shirt/index',array('shirts'=>$shirts, 'category' => $id));
	}

	public function View($id)
	{
		$shirt = Shirt::where('url', '=', $id)->get();
		if($shirt->count() > 0){
			$shirt[0]->clicks += 1;
			$shirt[0]->save();
		}
		return View::make('shirt/view',array('shirt'=>$shirt));
	}

	public function Add()
	{
		$input = Input::all();
		$urls = json_decode($input['shirts']);
		$response = array();
		$id = $input['id'];
		foreach ($urls as $url){
			$grab = $this->_Grab($url);
			array_push($response,$this->_Add($grab,$id));
		}
		return json_encode($response);
	}

	public function Edit()
	{
		$input = Input::all();
		$id = $input['shirt_id'];
		$url = $input['origin_url'];
		$grab = $this->_Grab($url);
		$response = $this->_Edit($grab,$id);
		return json_encode($response);
	}

	public function Delete()
	{
		$input = Input::all();
		$id = $input['shirt_id'];
		$shirt = Shirt::find($id);
		if($shirt->delete()){
			return json_encode(array('status'=>'success'));
		}else{
			return json_encode(array('status'=>'error'));
		}
	}

	public function _Grab($url)
	{
		$html = file_get_html($url);
		$name = $html->find('h1[class=clean]',0)->innertext;
		$description = $html->find('div[class=description]',0)->innertext;
		$img = $html->find('img[id=product-img]',0)->src;
		$sales = $html->find('span[class=amount-ordered]',0)->innertext;
		$parse['name'] = $name;
		$parse['description'] = $description;
		$parse['img'] = $img;
		$parse['url'] = $url;
		$parse['sales'] = $sales;
		return $parse;
	}

	public function _Add($parse,$id)
	{
		$shirt = new Shirt;
		$shirt->name = $parse['name'];
		$shirt->category_id = $id;
		$shirt->description = $parse['description'];
		$shirt->img = $parse['img'];
		$shirt->sales = $parse['sales'];
		$ur = md5(date('Y-m-d H:i:s'));
		$shirt->url = substr($ur, 0, 6);
		$shirt->origin_url = $parse['url'];
		if($shirt->save()){
			$sh['id'] = $shirt->id;
			$sh['name'] = $parse['name'];
			$sh['description'] = $parse['description'];
			$sh['url'] = substr($ur, 0, 6);
			$sh['origin_url'] = $parse['url'];
			$sh['status'] = 'success';
			return $sh;
		}else{
			$sh['status'] = 'error';
			return $sh;
		}
	}

	public function _Edit($parse,$id)
	{
		$shirt = Shirt::find($id);
		$shirt->name = $parse['name'];
		$shirt->description = $parse['description'];
		$shirt->img = $parse['img'];
		$shirt->sales = $parse['sales'];
		$ur = md5(date('Y-m-d H:i:s'));
		$shirt->url = substr($ur, 0, 6);
		$shirt->origin_url = $parse['url'];
		if($shirt->save()){
			$sh['id'] = $shirt->id;
			$sh['name'] = $parse['name'];
			$sh['description'] = $parse['description'];
			$sh['url'] = substr($ur, 0, 6);
			$sh['origin_url'] = $parse['url'];
			$sh['status'] = 'success';
			return $sh;
		}else{
			$sh['status'] = 'error';
			return $sh;
		}
	}

}
