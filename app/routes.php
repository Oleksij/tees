<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Admin route
Route::get('/admin', 'AdminController@Login');
Route::post('/admin', 'AdminController@Auth');
Route::get('/admin/logout', 'AdminController@Logout');
Route::get('/admin/home', array('before' => 'auth','uses'=>'AdminController@Home'));

	//Admin categories route
Route::post('/admin/category/add', array('before' => 'auth','uses'=>'CategoryController@Add'));
Route::get('/admin/category/stats/{id}', array('before' => 'auth','uses'=>'CategoryController@Stats'));
Route::get('/admin/category/edit/{id}', array('before' => 'auth','uses'=>'CategoryController@Edit'));
Route::post('/admin/category/edit/{id}', array('before' => 'auth','uses'=>'CategoryController@SaveEdit'));
Route::get('/admin/category/delete/{id}', array('before' => 'auth','uses'=>'CategoryController@Delete'));

	//Admin shirts route
Route::post('/admin/shirt/add',array('before' => 'auth','uses'=>'ShirtController@Add'));
Route::post('/admin/shirt/edit',array('before' => 'auth','uses'=>'ShirtController@Edit'));
Route::post('/admin/shirt/delete',array('before' => 'auth','uses'=>'ShirtController@Delete'));
Route::get('/admin/shirt/{id}', array('before' => 'auth','uses'=>'ShirtController@Index'));

//View shirt route
Route::get('/shirt/{id}', 'ShirtController@View');

//Home route
Route::get('/', 'HomeController@Index');

//Campaing view route
Route::get('/{id}', 'HomeController@Campaing');
