<?php

class CategoryController extends BaseController {

	public function Add()
	{
		$input = Input::all();
		$rules = array(
    		'name'  => 'required',
    		'slogan' => 'required',
    		'description' => 'required',
		);
		$validation = Validator::make($input, $rules);
		if($validation->passes()){
			$category = new Category;
			$category->name = $input['name'];
			$category->slogan = $input['slogan'];
			$category->description = $input['description'];
			$category->visits = 0;
			$url = md5(date('Y-m-d H:i:s'));
			$category->url = substr($url, 0, 6);
			$category->created = date('Y-m-d H:i:s');
			if($category->save()){
				return Redirect::to('admin/home')->with('success', 'Add success');
			}else{
				return Redirect::to('admin/home')->with('error', 'Add Failed');
			}
		}else{
			return Redirect::to('admin/home')->with('error', 'Field required');
		}
	}

	public function Edit($id)
	{
		$category = Category::findOrFail($id);
		return View::make('category/edit',compact('category'));
	}

	public function SaveEdit($id)
	{
		$category = Category::findOrFail($id);
		$input = Input::all();
		$rules = array(
    		'name'  => 'required',
    		'slogan' => 'required',
    		'description' => 'required',
		);
		$validation = Validator::make($input, $rules);
		if($validation->passes()){
			$category->name = $input['name'];
			$category->slogan = $input['slogan'];
			$category->description = $input['description'];
			if($category->save()){
				return Redirect::to('admin/home')->with('success', 'Add success');
			}else{
				return Redirect::to('admin/category/edit/'.$id)->with('error', 'Add Failed');
			}
		}else{
			return Redirect::to('admin/category/edit/'.$id)->with('error', 'Field required');
		}

		return View::make('category/edit',compact('category'));
	}

	public function Delete($id)
	{
		$category = Category::find($id);
		foreach ($category->shirts as $sh) {
			$sh->delete();
		}
		if($category->delete()){
			return Redirect::to('admin/home')->with('success', 'Delete success');
		}else{
			return Redirect::to('admin/home'.$id)->with('error', 'Delete Failed');
		}
	}

	public function Stats($id)
	{
		$shirts = Shirt::where('category_id', '=', $id)->get();
		$categories = Category::all();
		return View::make('category/stats',array('shirts'=>$shirts, "categories" => $categories, "currentCategory" => $id));
	}

}
