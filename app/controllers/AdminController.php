<?php

class AdminController extends BaseController {

	public function Home()
	{
		$categories = Category::all();
		return View::make('admin/home',array('categories'=>$categories));
	}

	public function Login()
	{
		return View::make('admin/login');
	}

	public function Auth()
	{
		$username = Input::get('username');
    	$password = Input::get('password');
 
    	if (Auth::attempt(array('username' => $username, 'password' => $password))) {
        	return Redirect::to('admin/home')->with('success', 'You have been logged in');
    	}
    	else {
        	return Redirect::to('admin')->with('error', 'Login Failed');
    	}

		return View::make('admin/login');
	}

	public function Logout()
	{
		Auth::logout();
		return Redirect::to('/')->with('success', 'Logout success');
	}

}
